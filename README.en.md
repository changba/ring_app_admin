# ring_app_admin

#### 软件架构

element-ui+vue+webpack

#### 安装教程

```
npm install
```

#### 使用说明

1. 开发

```
npm run dev
```

2. 生产

```
npm run build
```

#### 参与贡献

1. 新建基于 develop 的分支 Feat_xxx(开发者名称)
2. 在自己的分支上开发自己的 feature 分支
3. 汇总develop分支
4. 从develop新建 Pull Request到master

#### 链接地址
1. 原型地址
    https://i7s2s1.axshare.com/#g=1&p=001%E4%BA%A7%E5%93%81%E4%BB%8B%E7%BB%8D
2. 后台管理地址
    http://boss.lingsheng666.com
3. 效果图地址
    https://lanhuapp.com/url/PfQvJ
4. 接口文档地址
    https://apizza.net/pro/#/project/61223aef6e7bd183412ecca05012c7c0/browse
#### 路由使用简介（待优化）

```
path: '/',                      //路径
component: Home,                //组件
name: '运营管理',                //名称
iconCls: 'fa fa-id-card-o',     //图标
redirect: '/search',            //是否跳转以及跳转的路径
leaf: true,                     //是否为菜单项而非子菜单
hidden: true,                   //是否显示
children: [{}]                  //子路由 
```


#### host环境配置及书写api.js规范
- 在src/common/js/config.js更改开发环境和发布环境的ip地址

```
const ENV = process.env.NODE_ENV;
let devHost='xxx';
let proHost='xxx';
export default {
  host:(ENV==='development'?devHost:proHost),
  staticurl:(ENV==='development'?devHost:proHost)//静态资源地址
}
```


- 在src/api文件下新建自己的xx.api.js(以src/api/api.js为例,头部引入如下)

```
import axios from '../common/js/axiosTool';
import config from '../common/js/config';
let base = config.host;
```

#### 文件及目录说明

```
.
├── LICENSE
├── README.en.md //自述文件
├── README.md //自述文件
├── build  // 构建配置
│   ├── build.js
│   ├── check-versions.js
│   ├── dev-client.js
│   ├── dev-server.js
│   ├── utils.js
│   ├── vue-loader.conf.js
│   ├── webpack.base.conf.js
│   ├── webpack.dev.conf.js
│   └── webpack.prod.conf.js
├── config // 环境配置
│   ├── dev.env.js
│   ├── index.js
│   └── prod.env.js
├── dist // 打包目录
├── index.html //入口文件
├── package-lock.json
├── package.json // 依赖包信息
├── src
│   ├── App.vue
│   ├── addRoutes.js // 添加权限路由
│   ├── api // api.js
│   ├── assets // 静态资源
│   │   ├── bg1.jpg
│   │   ├── bg2.png
│   │   ├── logo.png
│   │   ├── logo4.png
│   │   ├── musicbg.jpg
│   │   ├── theme // 主题
│   │   │   ├── theme-darkblue
│   │   │   └── theme-green
│   │   └── user.png
│   ├── common// 通用文件
│   │   ├── css
│   │   │   └── global.scss // 通用样式
│   │   └── js
│   │       ├── axiosTool.js // axios拦截等配置工具
│   │       ├── center.js
│   │       ├── config.js // host配置
│   │       └── util.js // 封装的工具包，比如格式化时间，cookie相关操作等
│   ├── components // 组件
│   ├── main.js // 主文件
│   ├── mock //mock数据
│   │   ├── data
│   │   │   └── user.js
│   │   ├── index.js
│   │   └── mock.js
│   ├── routes.js // 静态路由
│   ├── styles
│   │   └── vars.scss
│   ├── utils // 工具包
│   │   ├── Base64.js
│   │   └── timeFormat.js
│   ├── views // 视图区
│   │   ├── 404.vue
│   │   ├── Home.vue
│   │   ├── Login.vue
│   │   ├── Main.vue
│   │   ├── Submenus.vue
│   │   ├── blacklisted_area
│   │   │   └── index.vue
│   │   ├── breadcrumb
│   │   ├── charts
│   │   │   └── echarts.vue
│   │   ├── nav1
│   │   │   ├── Form.vue
│   │   │   ├── Table.vue
│   │   │   ├── mock.js
│   │   │   ├── plays
│   │   │   │   ├── add.vue
│   │   │   │   ├── detail.vue
│   │   │   │   ├── index.vue
│   │   │   │   ├── list.vue
│   │   │   │   └── manage.vue
│   │   │   ├── ring
│   │   │   │   ├── detail.vue
│   │   │   │   ├── index.vue
│   │   │   │   ├── list.vue
│   │   │   │   └── manage.vue
│   │   │   ├── table
│   │   │   │   └── detail.vue
│   │   │   ├── tags
│   │   │   │   ├── detail.vue
│   │   │   │   ├── groupManage.vue
│   │   │   │   ├── index.vue
│   │   │   │   ├── list.vue
│   │   │   │   └── manage.vue
│   │   │   └── user.vue
│   │   ├── nav2
│   │   │   ├── Page4.vue
│   │   │   ├── Page5.vue
│   │   │   ├── Page6.vue
│   │   │   ├── advertisement
│   │   │   │   ├── adConfig.vue
│   │   │   │   ├── adManage.vue
│   │   │   │   └── index.vue
│   │   │   ├── cateList
│   │   │   │   ├── detail.vue
│   │   │   │   ├── edit.vue
│   │   │   │   ├── index.vue
│   │   │   │   └── list.vue
│   │   │   ├── edition
│   │   │   │   ├── editionDetail.vue
│   │   │   │   ├── editionList.vue
│   │   │   │   └── index.vue
│   │   │   ├── feedbacks
│   │   │   │   ├── feedback_detail.vue
│   │   │   │   ├── feedbacks_list.vue
│   │   │   │   └── index.vue
│   │   │   ├── indexList
│   │   │   │   ├── detailFixed.vue
│   │   │   │   ├── detailUnfixed.vue
│   │   │   │   ├── edit.vue
│   │   │   │   ├── index.vue
│   │   │   │   └── list.vue
│   │   │   ├── search
│   │   │   │   ├── hot
│   │   │   │   │   ├── detail.vue
│   │   │   │   │   ├── index.vue
│   │   │   │   │   └── list.vue
│   │   │   │   ├── index.vue
│   │   │   │   └── list
│   │   │   │       ├── detail.vue
│   │   │   │       ├── index.vue
│   │   │   │       └── list.vue
│   │   │   └── share
│   │   │       ├── index.vue
│   │   │       ├── shareDetail.vue
│   │   │       └── shareList.vue
│   │   ├── setup
│   │   │   ├── administrators
│   │   │   │   ├── detail.vue
│   │   │   │   ├── index.vue
│   │   │   │   ├── list.vue
│   │   │   │   └── power.vue
│   │   │   ├── log
│   │   │   │   └── index.vue
│   │   │   ├── nav
│   │   │   │   └── index.vue
│   │   │   └── parameter
│   │   │       ├── agreement.vue
│   │   │       ├── cache.vue
│   │   │       ├── components
│   │   │       │   └── tab.vue
│   │   │       ├── domain.vue
│   │   │       ├── engines.vue
│   │   │       ├── examine.vue
│   │   │       ├── index.vue
│   │   │       ├── index_x.vue
│   │   │       └── sensitive.vue
│   │   └── users
│   │       ├── detail.vue
│   │       ├── detail_components
│   │       │   ├── base_info.vue
│   │       │   ├── curUserNmae.vue
│   │       │   ├── modify_password.vue
│   │       │   ├── upload_bell.vue
│   │       │   ├── user_attention.vue
│   │       │   ├── user_comments.vue
│   │       │   ├── user_fans.vue
│   │       │   └── user_messages.vue
│   │       └── index.vue
│   └── vuex // 数据管理中心
│       ├── actions.js
│       ├── getters.js
│       └── store.js
├── static

```
