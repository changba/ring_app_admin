import babelpolyfill from 'babel-polyfill';
import Vue from 'vue';
import App from './App';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import VueRouter from 'vue-router';
import store from './vuex/store';
import Vuex from 'vuex';
import moment from 'moment';
import 'font-awesome/css/font-awesome.min.css';
import './common/css/global.scss';
import util from './common/js/util';
let { setCookie, getCookie } = util;
import { addRoutes } from './addRoutes.js';
import Base64 from './utils/Base64';
Vue.use(ElementUI);
Vue.use(VueRouter);
Vue.use(Vuex);
import { Message } from 'element-ui';
Vue.prototype.$moment = moment;
Vue.prototype.$Base64 = Base64;
// 初始路由只有登录页
let routes = [
    {
        path: '/login',
        component: () => import('./views/Login.vue'),
        name: '',
        hidden: true
    }
];
// 实例化路由
const router = new VueRouter({
    routes
});
// 未登录或者token过期 都要跳到登录页面
router.beforeEach((to, from, next) => {
    if(!getCookie('token') && from.path!=='/login'  && to.path!=='/login'){
        // 如果没有选中保存密码则清空信息
        if(!Vue.prototype.saveLogin){
            localStorage.login && localStorage.removeItem('login');
            localStorage.user && localStorage.removeItem('user');
        }
        Message({
            message: '登录已过期，需重新登录',
            type: 'error'
        });
    }
    if(!getCookie('token') && to.path !== '/login'){
        next('/login')
    }else{
        next()
    }
})
// 初始化app
let app = new Vue({
    router,
    store,
    mounted: function() {
        console.log('路由初始化...');
    },
    render: h => h(App)
}).$mount('#app');

// 刷新页面重新拉取权限路由
if(getCookie('token')){
    addRoutes(app,'reload')
}
