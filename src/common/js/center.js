import Vue from "vue";
import Axios from "axios";
import { Message } from "element-ui";
const isDev = window.location.origin.indexOf("http://localhost") === 0;
export var ringConfig = {
  API_HOST: isDev ? "http://47.100.199.106:10080" : window.location.origin,
  isDev
};

let APP = (window.APP = {});

Axios.interceptors.request.use(function(config) {
    if (
      config.url.indexOf("http://") !== 0 &&
      config.url.indexOf("https://") !== 0
    ) {
      config.url = ringConfig.API_HOST + config.url;
    }
    // 处理cookie 必须要
    //   config.withCredentials = true
    // config.headers = {
    // 	'Content-Type': 'application/json; charset=utf-8'
    // }
    if (config.method === "post" && !config.nopostChange) {
      config.transformRequest = function(data) {
        let ret = "";
        for (let it in data) {
          ret +=
            encodeURIComponent(it) + "=" + encodeURIComponent(data[it]) + "&";
        }
        return ret.substring(0, ret.length - 1);
      };
    }
    return config;
  },
  function(error) {
    return Promise.reject(error);
  }
);

// // Add a response interceptor
Axios.interceptors.response.use(function(response) {
  // if (data.isRedirect && data.redirectUrl) {
  //   window.location = data.redirectUrl.replace(/(back_url=)[^&]*/, '$1' + encodeURIComponent(window.location.href))
  //   return
  // }
	// 这里使用的是response是因为前面还有一个拦截器，它只返回了respone.data给我
  if (response.code != 200) {
    // 出错提醒
    Message({
      showClose: true,
      center: true,
      type: "error",
      message: response.message
    });
  }
  // 数据处理
  return response;
}, function(error) {
    // 出错提醒
    Message({
      showClose: true,
      center: true,
      type: "error",
      message: '服务器错误'
	});
	return Promise.reject(error);
});

var Ajax = function(url, data = {}, type = "get", nopostChange = true) {
  //   if (APP && APP.REQ_TOKEN) {
  //     data.token = APP.REQ_TOKEN
  //   }
  //   if (ringConfig.isDev) {
  //     data.isAjax = 1
  //     data.islogin = 1
  //     data.devUserId = 2856
  //   }
  //   if (ringConfig.isHttps) {
  //     data.isHttps = 1
  //   }
  if (type === "get") {
    return Axios({
      url,
      method: "get",
      params: data
    });
  } else if (type === "post" || type === "POST") {
    return Axios({
      url,
      method: "post",
      nopostChange,
      data
    });
  }
};

Vue.prototype.$ajax_ll = Ajax;

export { isDev, Axios as ajax, Ajax };
