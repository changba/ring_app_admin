import Vue from 'vue';
import axios from 'axios';
import qs from 'qs';
import { Message } from 'element-ui';
import envHost from './config';
import util from './util';
let { setCookie, getCookie, removeCookie } = util;
// response 非200 的code拦截白名单
// '/admin/role/getLoginPermissions'
let interceptorsWhiteList = [];

//取消请求
let CancelToken = axios.CancelToken;
//设置默认请求头，如果不需要可以取消这一步
axios.defaults.headers['Content-Type'] = 'application/x-www-form-urlencoded';
// 请求超时的时间限制
axios.defaults.timeout = 10000;
// 开始设置请求 发起的拦截处理
// config 代表发起请求的参数的实体
axios.interceptors.request.use(
    config => {
        let apiUrl = config.url.replace(envHost.host, '');
        // 除了登录页不需要token
        if (apiUrl !== '/admin/login') {
            // 如果有token就加上token
            if (getCookie('token')) {
                config.headers.Authorization = getCookie('token');
            } else {
                // 如果没有则要重新登录
                // 如果没有选中保存密码则清空信息
                if (!Vue.prototype.saveLogin) {
                    // 清空用户信息
                    localStorage.login && localStorage.removeItem('login');
                    localStorage.user && localStorage.removeItem('user');
                }
                Message({
                    message: '登录已过期，需重新登录',
                    type: 'error'
                });
                location.href = '#/login';
            }
        }
        // 得到参数中的 requestName 字段，用于决定下次发起请求，取消对应的 相同字段的请求
        // 如果没有 requestName 就默认添加一个 url
        let requestName;
        if (config.method === 'post') {
            config.data = qs.stringify(config.data);
            if (config.data && config.data.requestName) {
                requestName = config.data.requestName;
            } else {
                requestName = config.url;
            }
        } else {
            if (config.params && config.params.requestName) {
                requestName = config.params.requestName;
            } else {
                requestName = config.url;
            }
        }
        // 判断，如果这里拿到的参数中的 requestName 在上一次请求中已经存在，就取消上一次的请求
        if (axios[requestName] && axios[requestName].cancel) {
            axios[requestName].cancel('请勿频繁操作');
        }
        config.cancelToken = new CancelToken(c => {
            axios[requestName] = {};
            axios[requestName].cancel = c;
        });

        return config;
    },
    error => {
        return Promise.reject(error);
    }
);

// 请求到结果的拦截处理
axios.interceptors.response.use(
    config => {
        // 【code拦截】
        let code = config.data.code;
        let message = config.data.message;
        let apiUrl = config.config.url.replace(envHost.host, '');

        if (code !== 200) {
            // 未登录标识
            if (code == 101) {
                location.href = '#/login';
                // 干掉信息
                localStorage.getItem('user') && localStorage.removeItem('user');
                // 干掉token
                removeCookie('token');
            }
            // 不在白名单之列就要提示错误
            if (interceptorsWhiteList.indexOf(apiUrl) < 0) {
                // 处理错误信息
                Message({
                    message: message,
                    type: 'error'
                });
            }

            // 发生异常时也要去掉loading
            // TODO

            // 抛出异常
            return Promise.reject(message);
        }
        // 返回请求正确的结果
        return config.data;
    },
    error => {
        // 【status拦截】
        if (error && error.response) {
            switch (error.response.status) {
                case 400:
                    error.message = '错误请求';
                    break;
                case 401:
                    error.message = '未授权，请重新登录';
                    break;
                case 403:
                    error.message = '拒绝访问';
                    break;
                case 404:
                    error.message = '请求错误,未找到该资源';
                    break;
                case 405:
                    error.message = '请求方法未允许';
                    break;
                case 408:
                    error.message = '请求超时';
                    break;
                case 500:
                    error.message = '服务器端出错';
                    break;
                case 501:
                    error.message = '网络未实现';
                    break;
                case 502:
                    error.message = '网络错误';
                    break;
                case 503:
                    error.message = '服务不可用';
                    break;
                case 504:
                    error.message = '网络超时';
                    break;
                case 505:
                    error.message = 'http版本不支持该请求';
                    break;
                default:
                    error.message = `连接错误${error.response.status}`;
            }
        } else {
            !error.message && (error.message = '连接到服务器失败');
        }
        // 统一处理错误信息
        Message({
            message: error.message,
            type: 'error'
        });
        return Promise.reject(error.message);
    }
);
export default axios;
