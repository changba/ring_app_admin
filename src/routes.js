import Login from './views/Login.vue'
import NotFound from './views/404.vue'
import Home from './views/Home.vue'
import Main from './views/Main.vue'
import Table from './views/nav1/Table.vue'
import AdministratorsPower from './views/setup/administrators/power.vue';
import Log from './views/setup/log/index.vue';
import ParameterAgreement from './views/setup/parameter/agreement.vue';
import BlacklistedArea from './views/blacklisted_area/index.vue';
import User from './views/users/index.vue';
import UserDetail from './views/users/detail.vue';
let routes = [
    {
        path: '/login',
        component: Login,
        name: '',
        hidden: true
    },
    {
        path: '/404',
        component: NotFound,
        name: '',
        hidden: true
    },
    //{ path: '/main', component: Main },
    {
        path: '/',
        component: Home,
        name: '铃声管理',
        iconCls: 'el-icon-message', //图标样式class
        children: [
            { path: '/main', component: Main, name: '主页', hidden: true },
            { path: '/table', component: Table, name: '铃声库' },
            {
                path: '/table/detail/:rid(\\d+)',
                component: () => import('./views/nav1/table/detail.vue'),
                name: '铃声信息',
                hidden: true
            },
            {
                path: '/ring',
                component: () => import('./views/nav1/ring/index'),
                redirect: '/ring/manage',
                name: '铃声管理列表',
                children: [{
                    path: '/ring/manage',
                    component: () => import('./views/nav1/ring/manage'),
                    name: '铃声类型管理',
                }, {
                    path: '/ring/detail/:id(\\d+)?',
                    component: () => import('./views/nav1/ring/detail'),
                    name: '铃声详情',
                    hidden: true
                }, {
                    path: '/ring/list/:id(\\d+)?',
                    component: () => import('./views/nav1/ring/list'),
                    name: '铃声类型明细',
                    hidden: true
                }],
            },
            {
                path: '/plays',
                component: () => import('./views/nav1/plays/index'),
                redirect: '/plays/manage',
                name: '自定义歌单管理',
                children: [{
                    path: '/plays/manage',
                    component: () => import('./views/nav1/plays/manage'),
                    name: '歌单管理',
                }, {
                    path: '/plays/detail/:id(\\d+)?',
                    component: () => import('./views/nav1/plays/detail'),
                    name: '歌单详情',
                    hidden: true
                }, {
                    path: '/plays/list/:id(\\d+)?',
                    component: () => import('./views/nav1/plays/list'),
                    name: '歌单明细',
                    hidden: true
                },{
                    path: '/plays/add/:id(\\d+)?',
                    component: () => import('./views/nav1/plays/add'),
                    name: '批量添加铃声',
                    hidden: true
                }],
            },
            {
                path: '/tags',
                component: () => import('./views/nav1/plays/index'),
                redirect: '/tags/manage',
                name: '铃声标签管理',
                children: [{
                    path: '/tags/manage',
                    component: () => import('./views/nav1/tags/manage'),
                    name: '铃声标签管理',
                }, {
                    path: '/tags/detail/:id(\\d+)?',
                    component: () => import('./views/nav1/tags/detail'),
                    name: '铃声标签管理详情',
                    hidden: true
                }, {
                    path: '/tags/group/:id(\\d+)?',
                    component: () => import('./views/nav1/tags/groupManage'),
                    name: '铃声标签管理详情',
                    hidden: true
                }, {
                    path: '/tags/list/:id(\\d+)?',
                    component: () => import('./views/nav1/tags/list'),
                    name: '标签组歌曲明细',
                    hidden: true
                }],
            },
            {
                path: '/blacklist',
                component: BlacklistedArea,
                name: '黑名单地区'
            }
        ]
    },
    {
        path: '/',
        component: Home,
        name: '用户管理',
        iconCls: 'fa fa-address-card',
        leaf: true, //只有一个节点
        redirect: '/users',
        children: [
            {
                path: '/users',
                component: User,
                name: '用户列表',
                iconCls: 'fa fa-address-card',
                children: [
                    {
                        path: '/users/detail/:id',
                        component: UserDetail,
                        name: '用户信息',
                        iconCls: 'fa fa-address-card'
                    }
                ]
            },


        ]
    },
    {
        path: '/',
        component: Home,
        name: '运营管理',
        iconCls: 'fa fa-id-card-o',
        redirect: '/search',
        children: [{
            path: '/search',
            component: () => import('./views/nav2/search'),
            redirect: '/search/hot',
            name: '搜索管理',
            children: [{
                path: '/search/hot',
                component: () => import('./views/nav2/search/hot'),
                redirect: '/search/hot/list',
                name: '热搜词管理',
                leaf: true,
                children: [{
                    path: '/search/hot/list',
                    component: () => import('./views/nav2/search/hot/list'),
                    name: '热搜词列表',
                    hidden: true
                }, {
                    path: '/search/hot/detail/:id(\\d+)?',
                    component: () => import('./views/nav2/search/hot/detail'),
                    name: '热搜词详情',
                    hidden: true
                }]
            }, {
                path: '/search/list',
                component: () => import('./views/nav2/search/list'),
                redirect: '/search/list/list',
                name: '搜索榜单管理',
                leaf: true,
                children: [{
                    path: '/search/list/list',
                    component: () => import('./views/nav2/search/list/list'),
                    name: '搜索榜单列表',
                    hidden: true
                }, {
                    path: '/search/list/detail/:id(\\d+)?',
                    component: () => import('./views/nav2/search/list/detail'),
                    name: '搜索榜单热词管理',
                    hidden: true
                }]
            }]
        }, {
            path: '/indexList',
            component: () => import('./views/nav2/indexList'),
            name: '首页榜单管理',
            leaf: true,
            redirect: '/indexList/list',
            children: [{
                path: '/indexList/list',
                component: () => import('./views/nav2/indexList/list'),
                name: '首页榜单列表',
                hidden: true
            }, {
                path: '/indexList/edit/:id(\\d+)?',
                component: () => import('./views/nav2/indexList/edit'),
                name: '首页榜单编辑',
                hidden: true
            }, {
                path: '/indexList/detailFixed/:id(\\d+)',
                component: () => import('./views/nav2/indexList/detailFixed'),
                name: '固定榜单明细',
                hidden: true
            }, {
                path: '/indexList/detailUnfixed/:id(\\d+)',
                component: () => import('./views/nav2/indexList/detailUnfixed'),
                name: '非固定榜单明细',
                hidden: true
            }]
        }, {
            path: '/cateList',
            component: () => import('./views/nav2/cateList'),
            name: '分类榜单管理',
            redirect: '/cateList/list',
            leaf: true,
            children: [{
                path: '/cateList/list',
                component: () => import('./views/nav2/cateList/list'),
                name: '分类榜单列表',
                hidden: true
            }, {
                path: '/cateList/edit/:id(\\d+)?',
                component: () => import('./views/nav2/cateList/edit'),
                name: '分类榜单编辑',
                hidden: true
            }, {
                path: '/cateList/detail/:id(\\d+)',
                component: () => import('./views/nav2/cateList/detail'),
                name: '分类榜单明细',
                hidden: true
            }]
        },
        {
            path: '/advertisement',
            component: () => import('./views/nav2/advertisement'),
            name: '广告管理',
            redirect: '/advertisement/config',
            children: [
                {
                    path: '/advertisement/config',
                    component: () => import('./views/nav2/advertisement/adConfig'),
                    name: '广告配置'
                },
                {
                    path: '/advertisement/mange',
                    component: () => import('./views/nav2/advertisement/adManage'),
                    name: '第三方广告管理'
                }
            ]
        },
        {
            path: '/edition',
            component: () => import('./views/nav2/edition'),
            name: '版本管理',
            redirect: '/edition/list',
            children: [
                {
                    path: '/edition/list',
                    component: () => import('./views/nav2/edition/editionList'),
                    name: '版本列表'
                },
                {
                    path: '/edition/detail',
                    component: () => import('./views/nav2/edition/editionDetail'),
                    name: '版本详情'
                }
            ]
        },
        {
            path: '/share',
            component: () => import('./views/nav2/share'),
            name: '分享管理',
            redirect: '/share/list',
            children: [
                {
                    path: '/share/list',
                    component: () => import('./views/nav2/share/shareList'),
                    name: '分享文案列表'
                },
                {
                    path: '/share/detail',
                    component: () => import('./views/nav2/share/shareDetail'),
                    name: '文案详情'
                }
            ]
        },{
            path: '/feedbacks',
            component: () => import('./views/nav2/feedbacks'),
            name: '用户反馈',
            redirect: '/feedbacks/list',
            children: [
                {
                    path: '/feedbacks/list',
                    component: () => import('./views/nav2/feedbacks/feedbacks_list.vue'),
                    name: '用户反馈列表'
                },
                {
                    path: '/feedbacks/detail/:id(\d+)',
                    component: () => import('./views/nav2/feedbacks/feedback_detail.vue'),
                    name: '用户反馈详情'
                }
            ]
        }]
    },
    {
        path: '/',
        component: Home,
        name: '系统设置',
        iconCls: 'fa fa-bar-chart',
        children: [
            {
                path: '/setup/administrators',
                component: () => import('./views/setup/administrators/index.vue'),
                redirect: '/setup/administrators/list',
                name: '自定义管理员',
                hidden: true,
                children: [{
                    path: '/setup/administrators/list',
                    component: () => import('./views/setup/administrators/list.vue'),
                    name: '管理员列表'
                }, {
                    path: '/setup/administrators/detail/:id(\\d+)?',
                    component: () => import('./views/setup/administrators/detail.vue'),
                    name: '管理员详情',
                    hidden: true
                }]
            },
            {
                path: '/setup/administrators/list',
                component: () => import('./views/setup/administrators/list.vue'),
                name: '管理员列表',
            },
            { path: '/administrators/power', component: AdministratorsPower, name: '权限管理' },
            { path: '/setup/log', component: Log, name: '操作日志' },
            {
                name: '系统参数配置',
                path: '/setup/parameter',
                component: () => import('./views/setup/parameter/index'),
                children: [
                    { path: '/setup/parameter/domain', component: () => import('./views/setup/parameter/domain'), name: '域名设置' },
                    { path: '/setup/parameter/engines', component: () => import('./views/setup/parameter/engines'), name: '搜索引擎' },
                    { path: '/setup/parameter/cache', component: () => import('./views/setup/parameter/cache'), name: '缓存配置' },
                    { path: '/setup/parameter/agreement', component: ParameterAgreement, name: '注册协议配置' },
                    { path: '/setup/parameter/examine', component: () => import('./views/setup/parameter/examine'), name: '审核机制开关' },
                    { path: '/setup/parameter/sensitive', component: () => import('./views/setup/parameter/sensitive'), name: '审核关键词库' },
                ]
            }
        ]
    },
    // {
    //     path: '/',
    //     component: Home,
    //     name: '系统管理',
    //     iconCls: 'fa fa-bar-chart',
    //     children: [
    //         {path: '/echarts', component: echarts, name: '系统操作日志'},
    //         {path: '/page12', component: Page5, name: '系统基础参数'}
    //     ]
    // },
    {
        path: '*',
        hidden: true,
        redirect: { path: '/404' }
    }
];

export default routes;
