import axios from '../common/js/axiosTool';
import config from '../common/js/config';
let base = config.host;
// 获取日志列表
export const getLogList = params => {
    let {  ...param } = params;
    return axios
        .get(`${base}/admin/system/adminLog`, {
            params: param
        });
};
// 获取查询条件
export const getParams = params => {
    let {  ...param } = params;
    return axios
        .get(`${base}/admin/system/params`, {
            params: param
        });
};

