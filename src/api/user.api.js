import axios from '../common/js/axiosTool';
import config from '../common/js/config';
let base = config.host;
// 保存用户信息接口
export const updateUser = params => {
    let { id, ...param } = params;
    return axios.post(`${base}/admin/user/update/${id}`, param);
};
// 保修改用户密码接口
export const saveUserPass = params => {
    let { id, ...param } = params;
    return axios.post(`${base}/admin/user/updatePassword/${id}`, param);
};
// 用户关注列表接口
export const getFollowList = params => {
    let { id, ...param } = params;
    return axios.get(`${base}/admin/user/follow/${id}`, { params: param });
};
// 取消关注/移除粉丝接口
export const cancelFollow = params => {
    let { id } = params;
    return axios.get(`${base}/admin/user/cancelFollow/${id}`);
};
// 用户列表接口
export const getUserList = params => {
    return axios.get(`${base}/admin/user/index`, { params: params });
};
// 用户粉丝列表接口
export const getFansList = params => {
    let { id, ...param } = params;
    return axios.get(`${base}/admin/user/fans/${id}`, { params: param });
};
// 用户铃声列表接口
export const getBellList = params => {
    let { id, ...param } = params;
    return axios.get(`${base}/admin/user/ring/${id}`, { params: param });
};
// 用户信息接口
export const getUserDetail = params => {
    let { id } = params;
    return axios.get(`${base}/admin/user/info/${id}`);
};
// 锁定/解锁用户接口
export const lockUser = params => {
    let { id, ...param } = params;
    return axios.get(`${base}/admin/user/lock/${id}`, { params: param });
};
// 用户锁定记录列表接口
export const getLockLogs = params => {
    let { id, ...param } = params;
    return axios.get(`${base}/admin/user/getLockLog/${id}`, { params: param });
};
// 删除用户
export const deleteUser = params => {
    let { id, ...param } = params;
    return axios.get(`${base}/admin/user/del/${id}`, { params: param });
};

// 驳回发布铃声
export const cancelRealaseRing = params => {
    let { id, ...param } = params;
    return axios.post(`${base}/admin/ring/turnDown/${id}`, param);
};
// 发布铃声
export const realaseRing = params => {
    let { id, ...param } = params;
    return axios.get(`${base}/admin/ring/release/${id}`, {params: param});
};
// 保存锁定详情
export const saveLockLog = params => {
    let { id, ...param } = params;
    return axios.post(`${base}/admin/user/saveLockLog/${id}`, param);
};
// 铃声详情
export const getRingInfo = params => {
    let { id, ...param } = params;
    return axios.get(`${base}/admin/ring/info/${id}`);
};
// 保存铃声
export const saveRing = params => {
    let { id, ...param } = params;
    return axios.post(`${base}/admin/ring/edit/${id}`, param);
};
// 删除铃声接口
export const delRing = params => {
    let { id } = params;
    return axios.get(`${base}/admin/ring/del/${id}`);
};
// 用户导出
export const exportUser = params => {
    return axios.get(`${base}/admin/user/exportUser`,{params});
};
// 获取查询条件参数接口
export const getRingParams = params => {
    return axios.get(`${base}/admin/ring/param`);
};

