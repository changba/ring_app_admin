import axios from '../common/js/axiosTool';
import config from '../common/js/config';
let base = config.host;

const instance = axios.create({
    baseURL: base,
    timeout: 5000,
    headers: {
        'Content-Type': 'multipart/form-data'
    }
})

const cateListApi = {
    queryParams () {
        return axios.get(`${base}/admin/portalKeyword/params`)
    },
    cateList (params) {
        return axios.get(`${base}/admin/portalCategory/index`, { params })
    },
    cateOnOffline (id, params) {
        return axios.get(`${base}/admin/portalCategory/release/${id}`, { params })
    },
    cateShow (id) {
        return axios.get(`${base}/admin/portalCategory/show/${id}`)
    },
    cateSubmit (params) {
        return axios.post(`${base}/admin/portalCategory/store`, params)
    },
    cateDetail (id, params) {
        return axios.get(`${base}/admin/portalCategory/detail/${id}`, { params })
    },
    uploadFile (formData) {
        return instance.post('/admin/public/upload', formData)
    },
    listDelete (id) {
        return axios.get(`${base}/admin/portalCategory/del/${id}`)
    }

    // queryParams () {
    //     return axios.get(`${base}/admin/portalKeyword/params`)
    // },
    // listList (params) {
    //     return axios.get(`${base}/admin/portalTab/index`, { params })
    // },
    // listOnOffline (id, params) {
    //     return axios.get(`${base}/admin/portalTab/release/${id}`, { params })
    // },
    // listShow (id) {
    //     return axios.get(`${base}/admin/portalTab/show/${id}`)
    // },
    // listSubmit (params) {
    //     return axios.post(`${base}/admin/portalTab/store`, params)
    // },
    // listDetail (id) {
    //     return axios.get(`${base}/admin/portalTab/detail/${id}`)
    // }
}

export default cateListApi
