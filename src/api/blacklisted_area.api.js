import axios from '../common/js/axiosTool';
import config from '../common/js/config';
let base = config.host;
// 新增/修改黑名单接口
export const saveBlackArea = params => {
    return axios
        .post(`${base}/admin/contentBlock/store`, params);
};
// 黑名单列表接口
export const getBlackListArea = params => {
    return axios.get(`${base}/admin/contentBlock/index`, { params: params });
};
// 获取黑名单明细接口
export const getBlackAreaDetail = params => {
    let { id } = params;
    return axios.get(`${base}/admin/contentBlock/show/${id}`);
};
// 启动/停用接口
export const optBlackAreaDetail = params => {
    let { id, status } = params;
    return axios.get(`${base}/admin/contentBlock/release/${id}`, {
        params: { status }
    });
};
