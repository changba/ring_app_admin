import axios from '../common/js/axiosTool';
import config from '../common/js/config';
let base = config.host;

// 分享列表接口
export const getShareList = params => {
    return axios.get(`${base}/admin/share/index`, { params: params });
};

// 删除接口
export const deleteShare = params => {
    let { id } = params;
    return axios.get(`${base}/admin/share/del/${id}`);
};

// 上线/下线接口
export const updateStatus = params => {
    let { id, status } = params;
    return axios.get(`${base}/admin/share/release/${id}`, { 
        params: { status }
    });
};

// 获取分享信息接口
export const getShareDetail = params => {
    let { id } = params;
    return axios.get(`${base}/admin/share/show/${id}`);
};

// 新增分享文案接口
export const addShare = params => {
    return axios.post(`${base}/admin/share/store`, params);
};