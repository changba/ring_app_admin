import axios from '../common/js/axiosTool';
import config from '../common/js/config';
const base = config.host;

//提交铃声接口
const addRing = ({id, params}) => {
    return axios.post(`${base}/admin/playList/addRing/${id}`, params)
};
//删除歌单铃声接口
const delRing = id => {
    return axios.get(`${base}/admin/playList/delRing/${id}`);
};
//删除歌单接口
const delPlayList = id => {
    return axios.get(`${base}/admin/playList/del/${id}`)
};
//获取歌单明细
const getDetailList = ({id, params}) => {
    return axios.get(`${base}/admin/playList/detailList/${id}`, {params})
};
//获取自定义歌单列表接口
const getPlayLists = params => {
    return axios.get(`${base}/admin/playList/index`, {params})
};
//提交歌单编辑接口
const editPlaylist = ({id, params}) => {
    return axios.post(`${base}/admin/playList/edit/${id}`, params)
};
//获取歌单编辑信息接口
const getPlaylistDetail = id => {
    return axios.get(`${base}/admin/playList/edit/${id}`)
};
// 提交歌单新增接口
const addPlayList = params => {
    return axios.post(`${base}/admin/playList/add`,params)
};
//获取歌单新增ID
const getNewPlayListId = () => {
    return axios.get(`${base}/admin/playList/add`);
};
//歌单上下线接口
const releasePlays  = ({id, params}) => {
    return axios.get(`${base}/admin/playList/release/${id}`, {params})
};
//歌单请求参数
const getParamForRing  = () => {
    return axios.get(`${base}/admin/playList/getParamForRing`)
};

const batchAddRing = ({id, params}) =>{
    return axios.post(`${base}/admin/playList/batchAddRing/${id}`, params)
};
const exportPlayList = params =>{
    window.open(`${base}/admin/playList/exportPlayList?token=${params.token}&keyword=${params.keyword}`)
};
const exportPlayListRing = params =>{
    window.open(`${base}/admin/playList/exportPlayListRing?token=${params.token}&play_id=${params.play_id}`)
};

export {
    addRing,
    delRing,
    delPlayList,
    getDetailList,
    getPlayLists,
    editPlaylist,
    getPlaylistDetail,
    addPlayList,
    getNewPlayListId,
    releasePlays,
    batchAddRing,
    getParamForRing,
    exportPlayList,
    exportPlayListRing,
}