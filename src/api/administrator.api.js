import axios from '../common/js/axiosTool';
import config from '../common/js/config';
let base = config.host;

// 获取当前登录用户是否超级管理员接口
export const getSuperAdmin = params => {
    return axios
        .get(`${base}/admin/role/getSuperAdmin`);
};
// 查询管理员列表
export const getList = params => {
    let {  ...param } = params;
    return axios
        .get(`${base}/admin/admin/index`, {
            params: param
        });
};
// 查询管理员详情
export const getDetail = params => {
    let { id, ...param } = params;
    return axios
        .get(`${base}/admin/admin/show/${id}`, param);
};
// 获取管理员类型
export const getRoleList = params => {
    let {  ...param } = params;
    return axios
        .get(`${base}/admin/admin/getRole`, param);
};

// 新增或修改管理员接口
export const saveInfo = params => {
    let {  ...param } = params;
    return axios
        .post(`${base}/admin/admin/store`, param);
};
// 解锁/激活
export const updateStatus = params => {
    let {  id,status } = params;
    return axios
        .get(`${base}/admin/admin/updateStatus/${id}`, {
            params:{status}
        });
};

// 删除管理员
export const delRole = params => {
    let { id } = params;
    return axios
        .get(`${base}/admin/admin/del/${id}`);
};
