import axios from '../common/js/axiosTool';
import config from '../common/js/config';
let base = config.host;
// 获取角色权限组列表接口
export const getParameter = params => {
    let {...param} = params;
    return axios
        .get(`${base}/admin/system/show`, {
            params: param
        });
};
// 保存参数
export const saveParameter = params => {
    return axios
        .post(`${base}/admin/system/store`, params);
};


