import axios from '../common/js/axiosTool';
import config from '../common/js/config';
let base = config.host;

const indexListApi = {
    queryParams () {
        return axios.get(`${base}/admin/portalKeyword/params`)
    },
    listList (params) {
        return axios.get(`${base}/admin/portalTab/index`, { params })
    },
    listOnOffline (id, params) {
        return axios.get(`${base}/admin/portalTab/release/${id}`, { params })
    },
    listShow (id) {
        return axios.get(`${base}/admin/portalTab/show/${id}`)
    },
    listSubmit (params) {
        return axios.post(`${base}/admin/portalTab/store`, params)
    },
    listDetail (id, params) {
        return axios.get(`${base}/admin/portalTab/detail/${id}`, { params })
    },
    listDelete (id) {
        return axios.get(`${base}/admin/portalTab/del/${id}`)
    },
    listWeight (params) {
        return axios.post(`${base}/admin/ring/updateWeight`, params)
    }
}

export default indexListApi
