import axios from '../common/js/axiosTool';
import config from '../common/js/config';
let base = config.host;

// 版本列表接口
export const getVersionList = params => {
    return axios.get(`${base}/admin/version/index`, { params: params });
};

// 删除版本接口
export const deleteVersion = params => {
    let { id } = params;
    return axios.get(`${base}/admin/version/del/${id}`);
};

// 获取版本详情
export const getVersionDetail = params => {
    let { id } = params;
    return axios.get(`${base}/admin/version/show/${id}`);
};

// 新增/修改版本
export const saveVersionDetail = params => {
    return axios.post(`${base}/admin/version/store`, params)
};