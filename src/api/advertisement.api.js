import axios from '../common/js/axiosTool';
import config from '../common/js/config';
let base = config.host;

// 第三方广告列表接口
export const getAdList = params => {
    return axios.get(`${base}/admin/ads/index`, { params: params });
};

// 修改广告占比
export const updateRatio = params => {
    let { id, ratio } = params;
    return axios.get(`${base}/admin/ads/updateRatio/${id}`, { 
        params: { ratio }
    });
};