import axios from '../common/js/axiosTool';
import config from '../common/js/config';
let base = config.host;

const searchApi = {
    queryParams () {
        return axios.get(`${base}/admin/portalKeyword/params`)
    },
    queryHotParams () {
        return axios.get(`${base}/admin/portalSearchHot/params`)
    },
    // 搜索热词接口
    hotList (params) {
        return axios.get(`${base}/admin/portalKeyword/index`, { params });
    },
    hotDelete (id) {
        return axios.get(`${base}/admin/portalKeyword/del/${id}`)
    },
    hotOnOffline (id, params) {
        return axios.get(`${base}/admin/portalKeyword/release/${id}`, { params })
    },
    hotDetail (id) {
        return axios.get(`${base}/admin/portalKeyword/show/${id}`)
    },
    hotSubmit (params) {
        return axios.post(`${base}/admin/portalKeyword/store`, params)
    },
    // 搜索榜单接口
    listList (params) {
        return axios.get(`${base}/admin/portalSearchHot/index`, { params })
    },
    listLock (id, params) {
        return axios.get(`${base}/admin/portalSearchHot/lock/${id}`, { params })
    },
    listDelete (id) {
        return axios.get(`${base}/admin/portalSearchHot/del/${id}`)
    },
    listSubmit(params) {
        return axios.post(`${base}/admin/portalSearchHot/store`, params)
    },
    listDetail (id) {
        return axios.get(`${base}/admin/portalSearchHot/show/${id}`)
    }
}

export default searchApi
