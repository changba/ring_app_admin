import axios from '../common/js/axiosTool';
import config from '../common/js/config';
const base = config.host;

const getRingCategory = params => {
    return axios.get(`${base}/admin/ringCategory/index`, { params: params });
};
const deleteRingCategory = id => {
    return axios.get(`${base}/admin/ringCategory/del/${id}`);
};
const releaseRingCategory = ({id, status})=> {
    return axios.get(`${base}/admin/ringCategory/release/${id}`, { params: {status}});
};
const getNewCategoryId = () =>{
   return axios.get(`${base}/admin/ringCategory/add`)
};
const editCategory = ({params, id}) =>{
   return axios.post(`${base}/admin/ringCategory/edit/${id}`,params )
};
const getCategoryDetail = id =>{
   return axios.get(`${base}/admin/ringCategory/edit/${id}`)
};
const addCategory = (params) =>{
   return axios.post(`${base}/admin/ringCategory/add`, params )
};

const detailList = ({params, id}) =>{
    return axios.get(`${base}/admin/ringCategory/detailList/${id}`, { params: params });
};
//上传铃声接口
const uploadRing = params => {
    return axios.post(`${base}/admin/ring/uploadRing`,params)
};

export {
    getRingCategory,
    deleteRingCategory,
    releaseRingCategory,
    getNewCategoryId,
    editCategory,
    addCategory,
    detailList,
    getCategoryDetail,
    uploadRing,
}