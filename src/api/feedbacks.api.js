import axios from '../common/js/axiosTool';
import config from '../common/js/config';
let base = config.host;
// 反馈下拉框数据
export const getDealState = params => {
    return axios.get(`${base}/admin/feedback/tolist`);
};
// 反馈列表
export const getFeedbacksList = params => {
    let { id, ...param } = params;
    // status=0&keyword=xxxxx
    return axios.get(`${base}/admin/feedback/list`, { params: param });
};
// 删除反馈
export const delFeedback = params => {
    let { id, ...param } = params;
    return axios.get(`${base}/admin/feedback/delete?id=${id}`);
};
// 反馈详情
export const feedbackDetail = params => {
    let { id, ...param } = params;
    return axios.get(`${base}/admin/feedback/info?id=${id}`);
};
// 处理反馈
export const handleFeedback = params => {
    // id=3&content=xxxxx
    // content 不传递或则传递空 表示不处理
    // content 传递数据 表示已处理
    return axios.post(`${base}/admin/feedback/handler`, params);
};
// 添加反馈
export const addFeedback = params => {
    let { id, ...param } = params;
    // phone=13823298797&content=asdadasda
    return axios.get(`${base}/admin/feedback/add`, { params: param });
};
