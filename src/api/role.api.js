import axios from '../common/js/axiosTool';
import config from '../common/js/config';
let base = config.host;

// 获取当前登录用户是否超级管理员接口
export const getSuperAdmin = params => {
    return axios
        .get(`${base}/admin/role/getSuperAdmin`);
};
// 获取角色权限组列表接口
export const getRoleUserList = params => {
    let {  ...param } = params;
    return axios
        .get(`${base}/admin/role/index`, {
            params: param
        });
};
// 获取权限列表
export const getRoleList = params => {
    let {  ...param } = params;
    return axios
        .get(`${base}/admin/role/getPermissionList`, {
            params: param
        });
};
// 删除角色用户
export const delRole = params => {
    let {id} = params;
    return axios
        .get(`${base}/admin/role/del/${id}`, {
            params: {id}
        });
};
// 获取某个用户的权限
export const getUserRole = params => {
    let {id} = params;
    return axios
        .get(`${base}/admin/role/show/${id}`, {
            params: {id}
        });
};
// 新增、修改权限接
export const saveRole = params => {
    return axios
        .post(`${base}/admin/role/store`, params);
};



