import axios from '../common/js/axiosTool';
import config from '../common/js/config';
const base = config.host;

// 获取标签歌曲明细接口
const detailList = ({id,params}) => {
    return axios.get(`${base}/admin/ringTag/detailList/${id}`,{params});
};
//修改标签组状态接口
const updateGroupStatus = ({id,params}) => {
    return axios.get(`${base}/admin/ringTag/updateStatus/${id}`, {params})
};
//删除标签组和标签接口
const deleteTag = id => {
    return axios.get(`${base}/admin/ringTag/del/${id}`)
};
//提交标签组编辑接口
const editGroupTag = ({id, params}) => {
    return axios.post(`${base}/admin/ringTag/edit/${id}`, params)
};
//获取标签组编辑信息接口
const getTagGroupDetail = id => {
    return axios.get(`${base}/admin/ringTag/edit/${id}`)
};
//获取标签组编辑信息接口
const getTagInfo = id => {
    return axios.get(`${base}/admin/ringTag/editTag/${id}`)
};
//标签组提交新增接口
const addTagGroup = params => {
    return axios.post(`${base}/admin/ringTag/add`, params)
};
//标签组获取新增ID
const addTagGroupId = () => {
    return axios.get(`${base}/admin/ringTag/add`)
};
// 铃声标签列表接口
const getTagList = ({params}) => {
    return axios.get(`${base}/admin/ringTag/index`, {params});
};
//提交标签编辑接口
const editTag = ({id, params}) => {
    return axios.post(`${base}/admin/ringTag/addTag/${id}`, params);
};
//获取标签编辑信息接口
const getTagDetail = id => {
    return axios.get(`${base}/admin/ringTag/addTag/${id}`)
};

const addTag = params => {
    return axios.post(`${base}/admin/ringTag/addTag`,params)
};
const getTagId = () => {
    return axios.get(`${base}/admin/ringTag/addTag`)
};
const exportTag = params => {
    window.open(`${base}/admin/ringTag/exportTag?token=${params.token}&keyword=${params.keyword}`)
};
export {
    detailList,
    updateGroupStatus,
    deleteTag,
    editGroupTag,
    getTagGroupDetail,
    addTagGroup,
    addTagGroupId,
    getTagList,
    editTag,
    getTagDetail,
    getTagInfo,
    addTag,
    getTagId,
    exportTag,
}