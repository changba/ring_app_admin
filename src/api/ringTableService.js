import axios from '../common/js/axiosTool';
import config from '../common/js/config';
let base = config.host;

/**
 * 获取查询条件参数
 * @param {*} params 
 */
export const getFilterParams = params => {
    return axios.get(`${base}/admin/ring/param`, { params });
};

/**
 * 获取铃声列表接口
 * @param {*} params 
 */
export const getTableData = params => {
    return axios.get(`${base}/admin/ring/index`, { params });
};

/**
 * 铃声列表导出接口
 * @param {*} params 
 */
export const exportRing = params => {
    return axios.get(`${base}/admin/ring/exportRing`, { params });
};

/**
 * 发布铃声接口
 * @param {Number} id 铃声ID
 * @param {Object} params 
 */
export const releaseRing = (id, params) => {
    return axios.get(`${base}/admin/ring/release/${id}`, { params });
};

/**
 * 删除铃声接口
 * @param {Number} id 铃声ID
 * @param {Object} params 
 */
export const deleteRing = (id, params) => {
    return axios.get(`${base}/admin/ring/del/${id}`, { params });
};

/**
 * 铃声详情查询接口
 * @param {Number} id 铃声ID
 * @param {Object} params 
 */
export const getRingInfo = (id, params) => {
    return axios.get(`${base}/admin/ring/info/${id}`, { params });
};

/**
 * 铃声详情保存接口
 * @param {Number} id 铃声ID
 * @param {Object} params 
 */
export const editRingInfo = (id, params) => {
    return axios.post(`${base}/admin/ring/edit/${id}`, params);
};


/**
 * 铃声详情保存接口
 * @param {Number} id 铃声ID
 * @param {Object} params 
 */
export const turnDownRing = (id, params) => {
    return axios.post(`${base}/admin/ring/turnDown/${id}`, params);
};

/**
 * 上传歌词接口 
 */
export const uploadAndReturnContent = (id, params) => {
    return axios.post(`${base}/admin/public/uploadAndReturnContent`, params);
};