import axios from '../common/js/axiosTool';
import config from '../common/js/config';
let base = config.host;
// 后台登录接口
/**
 *
 * @param {username,password} params
 */
export const login = params => {
    return axios.post(`${base}/admin/login`, params);
};

// 后台登出接口
/**
 *
 * @param {username,password} params
 */
export const loginOut = params => {
    return axios.get(`${base}/admin/logout`);
};
// 修改密码接口
export const modifyPwd = (id, params) => {
    return axios.post(`${base}/admin/user/updatePassword/${id}`, params)
}
