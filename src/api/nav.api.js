import axios from '../common/js/axiosTool';
import config from '../common/js/config';
let base = config.host;

const navApi = {
    addNav (params) {
        return axios.post(`${base}/admin/role/addPermissions`, params)
    },
    getNav () {
        return axios.get(`${base}/admin/role/getLoginPermissions`)
    }
}

export default navApi
