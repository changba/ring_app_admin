import navApi from '@/api/nav.api.js';
const addChildren = function(item, route) {
    let subRoutes = [];
    if (item.child.length) {
        item.child.forEach(val => {
            let newRoute = {
                path: val.path,
                component: resolve => {
                    require([`./views/${val.component}`], resolve);
                },
                name: val.name,
                redirect: val.redirect,
                hidden: Boolean(val.hidden),
                iconCls: val.iconCls,
                leaf: Boolean(val.leaf)
            };
            if (val.child.length) {
                newRoute.children = addChildren(val, newRoute);
            }
            subRoutes.push(newRoute);
        });
    }
    return subRoutes;
};
export const addRoutes = (app,reload) => {
    navApi
        .getNav()
        .then(res => {
            if (res.result.length > 0) {
                let newRoutes = [];
                res.result.forEach(item => {
                    let route = {
                        path: item.path,
                        component: resolve => {
                            require([`./views/${item.component}`], resolve);
                        },
                        name: item.name,
                        redirect: item.redirect,
                        hidden: Boolean(item.hidden),
                        iconCls: item.iconCls,
                        leaf: Boolean(item.leaf)
                    };
                    if (item.child.length) {
                        route.children = addChildren(item, route);
                    }
                    newRoutes.push(route);
                });

                // 如果路由只有登录页
                if (app.$router.options.routes.length === 1) {
                    // 动态新增路由且跳到指定页
                    newRoutes.forEach(v => {
                        app.$router.options.routes.push(v);
                    });
                    app.$router.addRoutes(newRoutes);
                }
                !reload && app.$router.push({ path: '/table' });
            }
        })
        .catch(err => {});
};
