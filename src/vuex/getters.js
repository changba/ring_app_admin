//test
export const getCount = state => {
    return state.count
}
export const getSaveLoginMessage = state => {
    return state.saveLoginMessage
}
export const getUserTab = state => {
    return state.userTab
}
export const getCurUserName = state => {
    return state.curUserName
}