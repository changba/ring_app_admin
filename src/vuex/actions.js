//test
export const increment = ({ commit }) => {
    commit('INCREMENT');
};
export const decrement = ({ commit }) => {
    commit('DECREMENT');
};
// 设置用户管理-激活中的tab
export const saveUserTab = (context, data) => {
    context.state.userTab = data;
};
// 设置用户管理-当前用户名
export const saveCurUserName = (context, data) => {
    context.state.curUserName = data;
};
// 改变是否保存密码
export const changeSaveLoginMessage = (context, flag) => {
    context.state.saveLoginMessage = flag;
};
